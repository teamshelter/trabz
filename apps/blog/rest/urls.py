from django.conf.urls import url
from django.urls import include
from rest_framework.routers import DefaultRouter

from apps.blog.rest.views.views import PostView


router = DefaultRouter()
router.register(r'posts', PostView, base_name="posts")

urlpatterns = [
    url(r'', include(router.urls))

]

from rest_framework.viewsets import ModelViewSet

from apps.blog.models.models import Post
from apps.blog.rest.serializers.serializers import PostSerializer


class PostView(ModelViewSet):

    def get_queryset(self):
        return Post.objects.all()

    def get_serializer_class(self):
        return PostSerializer

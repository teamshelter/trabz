# -*- coding: utf-8 -*-
from rest_framework.serializers import ModelSerializer

from apps.blog.models.models import Post


class PostSerializer(ModelSerializer):
    """

    """

    class Meta:
        """

        """
        model = Post
        fields = '__all__'

from django.db import models
from django.utils.encoding import smart_text
from django.utils.translation import ugettext_lazy as _


class PublishedManager(models.Manager):
    """
    Returns published blog posts.
    """
    def get_queryset(self):
        queryset = super(PublishedManager, self).get_queryset()
        return queryset.filter(is_published=True)


class Post(models.Model):
    """
    Holds blog post data.
    """
    title = models.CharField(_("Name"), max_length=255)
    content = models.TextField()
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now_add=True)
    is_published = models.BooleanField(_("Published"), default=True)
    is_announcement = models.BooleanField(default=False)

    objects = models.Manager()
    published_objects = PublishedManager()

    class Meta:
        ordering = ("-date_created", )

    def __unicode__(self):
        return smart_text(self.title)
